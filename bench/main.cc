#include "benchmark/benchmark.h"
#include "array.h"

#define ELEM_SIZE sizeof(int)
#define ARRAY_SIZE (1024*ELEM_SIZE)

static void BM_array_set(benchmark::State& state) {
    // Perform setup here
    array_t a[array_reserve(ARRAY_SIZE, ELEM_SIZE)] = {0};
    int i = 0;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(i);
    
    if (array_init(a, ARRAY_SIZE, ELEM_SIZE)) {
        for (auto _ : state) {
        // This code gets timed
            array_set(a, i % ARRAY_SIZE, &i);
            i++;
        }
    }
}

static void BM_array_get(benchmark::State& state) {
    // Perform setup here
    array_t a[array_reserve(ARRAY_SIZE, ELEM_SIZE)] = {0};
    int i = 0;
    int e = 0;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(i);
    benchmark::DoNotOptimize(e);

    if (array_init(a, ARRAY_SIZE, ELEM_SIZE)) {
        for (auto _ : state) {
        // This code gets timed
            array_get(a, i % ARRAY_SIZE, &e);
            i++;
        }
    }
}

static void BM_array_peek(benchmark::State& state) {
    // Perform setup here
    array_t a[array_reserve(ARRAY_SIZE, ELEM_SIZE)] = {0};
    int i = 0;
    int* e = nullptr;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(i);
    benchmark::DoNotOptimize(e);
    
    if (array_init(a, ARRAY_SIZE, ELEM_SIZE)) {
        for (auto _ : state) {
        // This code gets timed
            array_peek(a, i % ARRAY_SIZE, (void**)&e);
            i++;
        }
    }
}

static void BM_raw_array_set(benchmark::State& state) {
    // Perform setup here
    int a[ARRAY_SIZE] = {0};
    int i = 0;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(i);

    for (auto _ : state) {
    // This code gets timed
        a[(i % ARRAY_SIZE)] = i;
        i++;
    }
}


static void BM_raw_array_get(benchmark::State& state) {
    // Perform setup here
    int a[ARRAY_SIZE] = {0};
    int i = 0;
    int e = 0;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(i);
    benchmark::DoNotOptimize(e);
    
    for (auto _ : state) {
    // This code gets timed
        e = a[i % ARRAY_SIZE];
        i++;
    }
}

static void BM_raw_array_peek(benchmark::State& state) {
    // Perform setup here
    int a[ARRAY_SIZE] = {0};
    int i = 0;
    int* e = nullptr;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(i);
    benchmark::DoNotOptimize(e);
    
    for (auto _ : state) {
    // This code gets timed
        e = a + (i % ARRAY_SIZE);
        i++;
    }
}

// Register the function as a benchmark
BENCHMARK(BM_array_set);
BENCHMARK(BM_raw_array_set);
BENCHMARK(BM_array_get);
BENCHMARK(BM_raw_array_get);
BENCHMARK(BM_array_peek);
BENCHMARK(BM_raw_array_peek);

// Run the benchmark
BENCHMARK_MAIN();