#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "array.h"

TEST_CASE("array reserve macro normal", "[array, array_reserve]") {
    SECTION( "array reserve macro normal" ) {
        const int size = 10;
        REQUIRE(array_reserve(size, int) == (sizeof(int) * size + sizeof(array_header_t)));
    }
    SECTION("array reserve macro zero") {
        const int size = 0;
        REQUIRE(array_reserve(size, int) == (sizeof(int) * size + sizeof(array_header_t)));
    }
    SECTION("array reserve macro negative") {
        const int size = -10000000;
        REQUIRE(array_reserve(size, int) == (sizeof(int) * (size_t)size + sizeof(array_header_t)));
    }
}

TEST_CASE("array init tests", "[array, array_init]") {
    const size_t size = 10;
    const size_t elem_size = sizeof(int);
    const char pattern = 'A';
    array_t a[array_reserve(size, int)];
    REQUIRE(NULL != memset(a, pattern, sizeof(a)));

    SECTION( "intialize array normally" ) {
        REQUIRE(true == array_init(a, size, elem_size));
        REQUIRE(((array_header_t*)a)->capacity == size);
        REQUIRE(((array_header_t*)a)->elem_size == elem_size);
        REQUIRE(((array_header_t*)a)->initialized == true);
        for (size_t i = sizeof(array_header_t); i < (sizeof(a) - sizeof(array_header_t)); i++) {
            REQUIRE(a[i] == pattern);
        }
    }
    SECTION( "intialize array with nullptr" ) {
        REQUIRE(false == array_init(NULL, size, elem_size));
        for (size_t i = 0; i < sizeof(a); i++) {
            REQUIRE(a[i] == pattern);
        }
    }
    SECTION( "intialize array zero element size" ) {
        REQUIRE(false == array_init(NULL, size, 0));
        for (size_t i = 0; i < sizeof(a); i++) {
            REQUIRE(a[i] == pattern);
        }
    }
    SECTION( "intialize array with zero size" ) {
        REQUIRE(true == array_init(a, 0, elem_size));
        REQUIRE(((array_header_t*)a)->capacity == 0);
        REQUIRE(((array_header_t*)a)->elem_size == elem_size);
        REQUIRE(((array_header_t*)a)->initialized == true);
        for (size_t i = sizeof(array_header_t); i < (sizeof(a) - sizeof(array_header_t)); i++) {
            REQUIRE(a[i] == pattern);
        }
    }
}

TEST_CASE("array deinit tests", "[array, array_deinit]") {
    const size_t size = 10;
    const char pattern = 'A';
    size_t pattern_field;
    array_t a[array_reserve(size, int)];
    REQUIRE(NULL != memset(a, pattern, sizeof(a)));
    REQUIRE(NULL != memset(&pattern_field, pattern, sizeof(pattern_field)));

    SECTION( "deintialize array normally" ) {
        REQUIRE(true == array_deinit(a));
        REQUIRE(((array_header_t*)a)->capacity == pattern_field);
        REQUIRE(((array_header_t*)a)->elem_size == pattern_field);
        REQUIRE(((array_header_t*)a)->initialized == false);
        for (size_t i = sizeof(array_header_t); i < (sizeof(a) - sizeof(array_header_t)); i++) {
            REQUIRE(a[i] == pattern);
        }
    }
    SECTION( "deintialize array with nullptr" ) {
        REQUIRE(false == array_deinit(NULL));
        for (size_t i = 0; i < sizeof(a); i++) {
            REQUIRE(a[i] == pattern);
        }
    }
}

TEST_CASE("array set tests", "[array, array_set]") {
    const size_t size = 10;
    const size_t elem_size = sizeof(int);
    const char pattern = 'A';
    size_t pattern_field;
    array_t a[array_reserve(size, int)];
    REQUIRE(NULL != memset(a, pattern, sizeof(a)));
    REQUIRE(NULL != memset(&pattern_field, pattern, sizeof(pattern_field)));
    REQUIRE(true == array_init(a, size, elem_size));

    SECTION( "set normal" ) {
        for (size_t i = 0; i < size; i++) {
            int e = i;
            REQUIRE(true == array_set(a, i, &e));
        }

        for (size_t ii = 0; ii < size; ii++) {
            int e = ii;
            REQUIRE(e == *(int*)(a + sizeof(array_header_t) + (elem_size * ii)));
        }
    }
    SECTION( "set with index of of range" ) {
        int i = 0;
        REQUIRE(false == array_set(a, size, &i));
        REQUIRE(false == array_set(a, -1, &i));
    }
    SECTION( "set with nullptr array" ) {
        int i = 0;
        REQUIRE(false == array_set(NULL, size - 1, &i));
    }
    SECTION( "set with nullptr element" ) {
        REQUIRE(false == array_set(a, size - 1, NULL));
    }
    SECTION( "set a deint array" ) {
        int i = 0;
        REQUIRE(true == array_deinit(a));
        REQUIRE(false == array_set(a, size - 1, &i));
    }
}

TEST_CASE("array get tests", "[array, array_get]") {
    const size_t size = 10;
    const size_t elem_size = sizeof(int);
    const char pattern = 'A';
    size_t pattern_field;
    array_t a[array_reserve(size, int)];
    REQUIRE(NULL != memset(a, pattern, sizeof(a)));
    REQUIRE(NULL != memset(&pattern_field, pattern, sizeof(pattern_field)));
    REQUIRE(true == array_init(a, size, elem_size));

    SECTION( "get normal" ) {
        for (size_t i = 0; i < size; i++) {
            int e = i;
            REQUIRE(true == array_set(a, i, &e));
        }

        for (size_t ii = 0; ii < size; ii++) {
            int elem = 0;
            REQUIRE(true == array_get(a, ii, &elem));
            REQUIRE(ii == elem);
        }
    }
    SECTION( "get with index of of range" ) {
        int i = 0;
        REQUIRE(false == array_get(a, size, &i));
        REQUIRE(false == array_get(a, -1, &i));
    }
    SECTION( "get with nullptr array" ) {
        int i = 0;
        REQUIRE(false == array_get(NULL, size, &i));
    }
    SECTION( "get with nullptr element" ) {
        REQUIRE(false == array_get(a, size, NULL));
    }
    SECTION( "get with deinit array" ) {
        int i = 0;
        REQUIRE(true == array_deinit(a));
        REQUIRE(false == array_get(a, size, &i));
    }
}

TEST_CASE("array peek tests", "[array, array_peek]") {
    const size_t size = 10;
    const size_t elem_size = sizeof(int);
    const char pattern = 'A';
    size_t pattern_field;
    array_t a[array_reserve(size, int)];
    REQUIRE(NULL != memset(a, pattern, sizeof(a)));
    REQUIRE(NULL != memset(&pattern_field, pattern, sizeof(pattern_field)));
    REQUIRE(true == array_init(a, size, elem_size));

    SECTION( "peek at array normal" ) {
        for (size_t i = 0; i < size; i++) {
            int e = i;
            REQUIRE(true == array_set(a, i, &e));
        }

        for (size_t ii = 0; ii < size; ii++) {
            int* elem = NULL;
            REQUIRE(true == array_peek(a, ii, (void**)&elem));
            REQUIRE((int)ii == *elem);
        }
    }
    SECTION( "peek at array with index of of range" ) {
        int* i = NULL;
        REQUIRE(false == array_peek(a, size, (void**)&i));
        REQUIRE(false == array_peek(a, -1, (void**)&i));
    }
    SECTION( "peek at array with nullptr array" ) {
        int* i = NULL;
        REQUIRE(false == array_peek(NULL, size, (void**)&i));
    }
    SECTION( "peek at array with nullptr element" ) {
        REQUIRE(false == array_peek(a, size, NULL));
    }
    SECTION( "peek at array with nullptr element" ) {
        int* i = NULL;
        REQUIRE(true == array_deinit(a));
        REQUIRE(false == array_peek(a, size, (void**)&i));
    }
}

TEST_CASE("array capacity tests", "[array, array_capacity]") {
    const size_t size = 10;
    const size_t elem_size = sizeof(int);
    const char pattern = 'A';
    size_t pattern_field;
    array_t a[array_reserve(size, int)];
    REQUIRE(NULL != memset(a, pattern, sizeof(a)));
    REQUIRE(NULL != memset(&pattern_field, pattern, sizeof(pattern_field)));
    REQUIRE(true == array_init(a, size, elem_size));

    SECTION("get the capacity of a normal array") {
        size_t capacity;
        REQUIRE(true == array_capacity(a, &capacity));
        REQUIRE(size == capacity);
    }
    SECTION("get the capacity of a deinitialized array") {
        size_t capacity;
        REQUIRE(true == array_deinit(a));
        REQUIRE(false == array_capacity(a, &capacity));
    }
    SECTION("get the capacity of a nullptr array") {
        size_t capacity;
        REQUIRE(false == array_capacity(nullptr, &capacity));
    }
    SECTION("get the capacity of a array with nullptr capacity") {
        REQUIRE(false == array_capacity(a, NULL));
    }
}