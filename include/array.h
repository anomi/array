#ifndef _ARRAY_H_
#define _ARRAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/**
 * @brief header struct which is located before the data
 * 
 */
typedef struct array_header_s {
    size_t elem_size;   /**< size of one element */
    size_t capacity;    /**< number of elements the array can hold */
    bool initialized;   /**< flag if the array_init function was used */
} array_header_t;

typedef uint8_t array_t;

/**
 * @brief helper macro to reserve the corret amount of data
 * 
 */
#define array_reserve(num, type) (((size_t)(num)) * sizeof(type) + sizeof(array_header_t))

/**
 * @brief initialize the header of the array so it can be used
 * 
 * @param[in] ptr pointer to the memory block which should be used
 * @param size number of elements the block can take (without the header)
 * @param[in] elem_size size of one element
 * @return true on success
 */
static inline bool array_init(array_t* ptr, size_t size, size_t elem_size) {
    array_header_t* h = (array_header_t*)ptr;
    bool b = false;
    if (h != NULL
        && elem_size >= 1) {
        h->capacity = size;
        h->elem_size = elem_size;
        h->initialized = true;
        b = true;
    }
    return b;
}

/**
 * @brief invalidate the header so it can not be used any longer
 * 
 * @param[in] ptr pointer to the memory block which is used
 * @return true on success
 */
static inline bool array_deinit(array_t* ptr) {
    array_header_t* h = (array_header_t*)ptr;
    bool b = false;
    if (h != NULL) {
        h->initialized = false;
        b = true;
    }
    return b;
}

/**
 * @brief add a element to a specific location in the array
 * 
 * @param[in] ptr pointer to the memory block which is used
 * @param[in] index index of the element to access
 * @param[in] e pointer from where the element should be copied
 * @return true on success
 */
static inline bool array_set(array_t* ptr, size_t index, void* e) {
    bool b = false;
    array_header_t* h = (array_header_t*)ptr;
    if (ptr != NULL
        && (h->capacity - 1) >= index
        && h->initialized == true
        && e != NULL) {
        uint8_t* data = ((uint8_t*)ptr) +  sizeof(array_header_t); 
        b = memcpy(&data[index * h->elem_size], e, h->elem_size) != NULL;
    }
    return b;
}

/**
 * @brief read a element of the array and copies it to e
 * 
 * @param[in] ptr pointer to the memory block which is used
 * @param[in] index index of the element to access
 * @param[out] e pointer where the element should be copied
 * @return true on success
 */
static inline bool array_get(array_t* ptr, size_t index, void* e) {
    bool b = false;
    array_header_t* h = (array_header_t*)ptr;
    if (ptr != NULL
        && (h->capacity - 1) >= index
        && h->initialized == true
        && e != NULL) {
        uint8_t* data = ((uint8_t*)ptr) +  sizeof(array_header_t); 
        b = memcpy(e, &data[index * h->elem_size], h->elem_size) != NULL;
    }
    return b;
}

/**
 * @brief read a element of the array and return the pointer to the element
 * 
 * @param[in] ptr pointer to the memory block which is used
 * @param[in] index index of the element to access
 * @param[out] e pointer of the pointer where the location should be stored
 * @return true on success 
 */
static inline bool array_peek(array_t* ptr, size_t index, void** e) {
    bool b = false;
    array_header_t* h = (array_header_t*)ptr;
    if (ptr != NULL
        && (h->capacity - 1) >= index
        && h->initialized == true
        && e != NULL) {
        uint8_t* data = ((uint8_t*)ptr) +  sizeof(array_header_t);
        *e =  (void*) &data[index * h->elem_size];
        b = true;
    }
    return b;
}

/**
 * @brief get the number of element the array can store
 * 
 * @param[in] ptr pointer to the memory block which is used
 * @param[out] capacity pointer where the capacity should be stored
 * @return true on success
 */
static inline bool array_capacity(array_t* ptr, size_t* capacity) {
    bool b = false;
    array_header_t* h = (array_header_t*)ptr;
    if (ptr != NULL
        && h->initialized == true
        && capacity != NULL) {
        *capacity = h->capacity;
        b = true;
    }
    return b;
}

#ifdef __cplusplus
}
#endif

#endif // _ARRAY_H_