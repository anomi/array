# What is Array

Array is a header only, high level implementation to access c arrays.

## How to Use

```C
#include <stdio.h>
#include "array.h"

int main(int argc, char const *argv[]) {
    // create the array
    array_t fanzy_array[array_reserve(10, int)];
    // init it
    (void)array_init(fanzy_array, 10, sizeof(int));

    // fill it with data
    for (int i = 0; i < 10; i++) {
        (void)array_set(fanzy_array, i, &i);
    }

    // read data back && print it
    for (int i = 0; i < 10; i++) {
        int* e;
        (void)array_peek(fanzy_array, i, (void**)&e);
        printf("%d \n", *e);
    }

    return 0;
}
```

## How to Build

The library itself does not need to be build, because it is header only, so you could just copy the header file or use cmake "add_subdirecotry" to add it to your project.

To build the tests and the benchmark there are two cmake options for that `ARRAY_BUILD_TESTS` and `ARRAY_BUILD_BENCHMARK`. By default this two options are enabled.

## Benchmark

I made a simple benchmark to compare the performace impact between the array implemenation and the raw array. Below you can find the results on my machine with -O0 compilation.

```bash
2019-09-29 12:48:36
Running ./bench/run_bench
Run on (8 X 3600 MHz CPU s)
CPU Caches:
  L1 Data 32K (x4)
  L1 Instruction 32K (x4)
  L2 Unified 256K (x4)
  L3 Unified 6144K (x1)
Load Average: 1.61, 1.43, 1.63
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
***WARNING*** Library was built as DEBUG. Timings may be affected.
------------------------------------------------------------
Benchmark                  Time             CPU   Iterations
------------------------------------------------------------
BM_array_set            9.80 ns         9.78 ns   1000000000
BM_raw_array_set        2.94 ns         2.93 ns   1000000000
BM_array_get            10.3 ns         10.3 ns   1000000000
BM_raw_array_get        3.03 ns         3.01 ns   1000000000
BM_array_peek           7.82 ns         7.80 ns   1000000000
BM_raw_array_peek       2.99 ns         2.98 ns   1000000000
```
